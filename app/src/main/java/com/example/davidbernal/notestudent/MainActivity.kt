package com.example.davidbernal.notestudent

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {

    lateinit var ingrese_Name:EditText
    lateinit var butonSiguiente:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ingrese_Name = findViewById(R.id.ingrese_nick)

        recuperarDatos()

        butonSiguiente = findViewById(R.id.button_siguiente)

        butonSiguiente.setOnClickListener {

            guardarDatos()

            val intent = Intent(this,NavigationBar_Activity::class.java);
            startActivity(intent);
        }

        elegir_imagen.setOnClickListener {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)==
                        PackageManager.PERMISSION_DENIED){
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE);

                    requestPermissions(permissions, PERMISSION_CODE);
                }else{
                    pickImageFromGallery()
                }
            }else{
                pickImageFromGallery()
            }
        }
    }


    private fun pickImageFromGallery(){

        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    companion object {
        private val IMAGE_PICK_CODE=1000;
        private val PERMISSION_CODE=1001;
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery()
                }else{
                    Toast.makeText(this,"Permiso Denegado",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imagen_perfil.setImageURI(data?.data)
        }
    }

    private fun recuperarDatos(){
        val mypref = getSharedPreferences("mypref", Context.MODE_PRIVATE)
        val name = mypref.getString("name","")

        ingrese_Name.setText(name)
    }

    private fun guardarDatos() {
        if (ingrese_Name.text.isEmpty()){
            ingrese_Name.error = "Por favor ingresa un nombre"
            return
        }

        val mypref = getSharedPreferences("mypref", Context.MODE_PRIVATE)

        val editor = mypref.edit()

        editor.putString("name",ingrese_Name.text.toString())

        editor.apply()

        Toast.makeText(this,"Datos Guardados", Toast.LENGTH_LONG).show()

    }

}
