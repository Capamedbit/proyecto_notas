package com.example.davidbernal.notestudent

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.davidbernal.notestudent.R.id.navigation_materia
import kotlinx.android.synthetic.main.activity_navigation_bar_.*

class NavigationBar_Activity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_materia -> {
                message.setText(R.string.title_materia)

                val materiasFragments = MateriasFragments.newInstance()
                openFragment(materiasFragments)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_calendario -> {
                //message.setText(R.string.title_calendario)
                val calendarioFragments = CalendarioFragments.newInstance()
                openFragment(calendarioFragments)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_configuracion -> {
                message.setText(R.string.title_configuracion)
                val configuracionFragments = ConfiguracionFragments.newInstance()
                openFragment(configuracionFragments)
                return@OnNavigationItemSelectedListener true
            }

        }
        false
    }


    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_bar_)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}

