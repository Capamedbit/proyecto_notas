package com.example.davidbernal.notestudent

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.support.v4.app.FragmentTransaction


class ConfiguracionFragments : Fragment() {

    lateinit var buttonAyuda : Button
    lateinit var buttonAcerca : Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        val vi = inflater.inflate(R.layout.activity_configuracion_fragments, container, false)

        buttonAyuda = vi.findViewById(R.id.button_ayuda)
        buttonAcerca = vi.findViewById(R.id.button_acerca)

        buttonAyuda.setOnClickListener{

            val intent = Intent(context,verAyuda_fragment::class.java);
            startActivity(intent);
        }

        buttonAcerca.setOnClickListener {
            val intent = Intent(context,Ver_Acerca::class.java);
            startActivity(intent);
        }
        return vi;
    }

    companion object {
        fun newInstance(): ConfiguracionFragments = ConfiguracionFragments()
    }
}