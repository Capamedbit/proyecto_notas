package com.example.davidbernal.notestudent

import android.os.Bundle
import android.widget.CalendarView
import android.widget.TextView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_calendario_fragments.*

class CalendarioFragments : Fragment() {

    lateinit var mCalendarView: CalendarView
    lateinit var textView: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val vi = inflater.inflate(R.layout.activity_calendario_fragments, container, false)

        mCalendarView = vi.findViewById(R.id.calendar);
        textView=vi.findViewById(R.id.textito)

        mCalendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            // Note that months are indexed from 0. So, 0 means January, 1 means february, 2 means march etc.
            val messageDate = ""+dayOfMonth + "/" + (month + 1) + "/" + year
            display(messageDate);
        }
        return vi;
    }

    private fun display(messageDate:String){
        textView.setText("" + messageDate)
    }

    companion object {
        fun newInstance(): CalendarioFragments = CalendarioFragments()
    }
}