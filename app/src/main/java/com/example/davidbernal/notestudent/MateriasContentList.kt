package com.example.davidbernal.notestudent

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.widget.EditText
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_materias_content_list.*
import java.util.*

class MateriasContentList : AppCompatActivity() {

    lateinit var listRecyclerView: RecyclerView;
    val dataBase = FirebaseDatabase.getInstance();
    val ref = dataBase.getReference("list-materias");

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_materias_content_list)
        val listId = intent.getStringExtra(MateriasFragments.INTENT_LIST_ID);
        val ref2 = dataBase.getReference("list-materias/" + listId + "/list");
        ref.child(listId).child("list-name")
                .addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError) {

                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        title = dataSnapshot.value.toString();
                    }

                })



        listRecyclerView = findViewById(R.id.materias_recycler_view_content);
        listRecyclerView.layoutManager = LinearLayoutManager(this);
        listRecyclerView.adapter = MateriaContentListAdapter(this, ref2);


        fab2.setOnClickListener { view ->
            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //   .setAction("Action", null).show()
            showCreateListDialog(listId);
        }

    }

    private fun showCreateListDialog(listId: String) {
        val dialogTitle = getString(R.string.name_of_list);
        val positiveButtonTitle = getString(R.string.create_list);

        val builder = AlertDialog.Builder(this);
        val listTitleEditText = EditText(this);
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT;

        builder.setTitle(dialogTitle);
        builder.setView(listTitleEditText);

        builder.setPositiveButton(positiveButtonTitle) { dialog, i ->

            val newList = listTitleEditText.text.toString();
            val newId = UUID.randomUUID().toString();
            //ref.setValue(newlist);
            //ref.child(newId).child("list-name").setValue(newList)
            ref.child(listId).child("list").child(newId).child("list-name-list").setValue(newList);
            dialog.dismiss();
            //showListDetail(newId);
        }

        builder.create().show();
    }
}