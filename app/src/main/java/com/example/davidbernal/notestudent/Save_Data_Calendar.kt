package com.example.davidbernal.notestudent

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Save_Data_Calendar : AppCompatActivity() {

    lateinit var ButtonEdit : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save__data__calendar)

        ButtonEdit = findViewById(R.id.buttonEditAlarm)

        ButtonEdit.setOnClickListener {

            val intent = Intent(this, Alarm_Activity::class.java)
            startActivity(intent)

        }
    }



}
