package com.example.davidbernal.notestudent

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.*
import android.widget.Button
import android.widget.EditText
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.activity_materias_fragments.*
import kotlinx.android.synthetic.main.activity_materias_fragments.view.*
import java.util.*

class MateriasFragments : Fragment(),  MateriasAdapter.ListSelectionRecyclerViewClickListener
{

    lateinit var listReciclerView: RecyclerView

    var database = FirebaseDatabase.getInstance()
    var ref = database.getReference("list-materias")



    companion object {
        val INTENT_LIST_ID = "listID"
        fun newInstance(): MateriasFragments = MateriasFragments()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val vi = inflater.inflate(R.layout.activity_materias_fragments, container, false)

        listReciclerView= vi.findViewById(R.id.materias_recicler_view)
        listReciclerView.layoutManager= LinearLayoutManager(context)
        listReciclerView.adapter =MateriasAdapter(ref,this)

        vi.fab.setOnClickListener{view ->
            showCreateDialog()}

        return vi;
    }

    /*fun eliminarDato(){

        ref.child("list-name").child("listName").removeValue()
    }
*/
    fun showCreateDialog(){

        val dialogTitle =getString(R.string.name_of_list) // sacando los strings

        val positiveButtonTitle =  getString(R.string.create_list) // .. sacando los strings

        val  builder = AlertDialog.Builder(context!!) // builder para nuestro alerta
        val listTitleEditText = EditText(context) // creando una vista de tipo edit texto
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT


        builder.setTitle(dialogTitle)
        builder.setView(listTitleEditText)


        builder.setPositiveButton(positiveButtonTitle) {dialog, i ->

            val newList= listTitleEditText.text.toString() //L7
            //val id = ref.push().key.toString()

            //val nuevaMateria = ListaMateria(id,nombreMateria)
            val newId = UUID.randomUUID().toString()
            //ref.child(newId)
            // ref.setValue(newList) //L7
            //ref.child(id).child("list-name").setValue(nuevaMateria)
            ref.child(newId).child("list-name").setValue(newList)//L7 con el .child, estamos creando como una indentacion mas en la base de datos // esto base de datos por archivo
            //siempre que queremos geerear id con el codigo usamos ese UUID que es un algoritmo raro que saca un id que no se va a repetir ,
            //dentro de ese vamos a crear un list-name que es la llave del valor que voy a setear en newList

            dialog.dismiss()
            //showListDetail(newId)
        }
        builder.create().show()
    }

    private fun showContentList(listId:String){ //1L14
        val ContentListIntent = Intent(context, MateriasContentList::class.java) //crear el intent, estoy en este activity y quiero ir al otro activity

        ContentListIntent.putExtra(INTENT_LIST_ID, listId) //puedo enviar el id al siguiente activity, es decir nos permite enviar informacion a otro actibity // la llamamos cuando toquemos un item de la lista

        startActivity(ContentListIntent) //lanza la siguiente actividad o activity
    }

    override fun listenerItemClicked(todoList:ListaMateria ) { //que queremos hacer cuando le demos clic en cada uno de los items
        showContentList(todoList.id)
    }
}