package com.example.davidbernal.notestudent

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference


class MateriasAdapter(ref:DatabaseReference, val clickListener: ListSelectionRecyclerViewClickListener ): //5L14
        RecyclerView.Adapter<MateriasHolder>() {

    interface ListSelectionRecyclerViewClickListener {
        fun listenerItemClicked(todoList: ListaMateria)
    }


    var todoLists: ArrayList<ListaMateria> = arrayListOf();
    internal lateinit var item_Card_View: CardView

    init {
        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(item: DatabaseError) {

            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {

            }

            override fun onChildRemoved(item: DataSnapshot) { //cuando se elimina nos devuelve u daro en est funcion
                val deletedIndex = todoLists.indexOfFirst { it.id == item.key} //estoy encontrando el indice del key que se elimino// est} todoList.remove()b//id como lollamamos en kotlin y key eso largaso
                //indexOfFirs se ejecuta muchas veces, se busca en muchos items esa codicion
                // otra forma de escribir lo anterior es indexOfFirst{ element -> element.id == item.key
                todoLists.removeAt(deletedIndex)
                notifyItemRemoved(deletedIndex)
            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val listTitle = item.child("list-name").value.toString() //tengo la refencia en item a la referencia de firebase
                val listId = item.key.toString()
                //val color_materia = item.child("color").value.toString()

                todoLists.add(ListaMateria(listId, listTitle))
                //todoLists.add(ListaMateria(listId, listTitle, color_materia))
                notifyItemInserted(todoLists.size) //para decirle al adapter que su data cambio para que se actualice

            }

        })
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): MateriasHolder {


        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.materias_holder, parent, false)

        item_Card_View = view.findViewById(R.id.cardview_item)

        return MateriasHolder(view)


    }

    override fun getItemCount(): Int {
        return todoLists.count()
    }


    //OnBindViewHolder
    //es mostrar los datos de una posición especificada,
    // también configura el contenido de las vistas y
    // actualiza los datos de un ViewHolder ya
    // existente.

    override fun onBindViewHolder(
            holder: MateriasHolder,
            position: Int) {

        holder.listTitle.text = todoLists[position].listName;

        /* val color1="#"+todoLists[position].color
         item_Card_View.setBackgroundColor(Color.parseColor(color1));*/


        //item_Card_View.setBackgroundColor(Color.RED)
        holder.itemView.setOnClickListener {
            clickListener.listenerItemClicked(todoLists[position]);


        }
    }
}