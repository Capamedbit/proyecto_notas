package com.example.davidbernal.notestudent

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.widget.CalendarView
import android.widget.EditText

import android.widget.TextView
import android.widget.Toast

class Calendar_Activity : AppCompatActivity() {

    lateinit var textView:TextView;
    lateinit var calendarView: CalendarView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)


        calendarView = findViewById(R.id.calendar);

        calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            // Note that months are indexed from 0. So, 0 means January, 1 means february, 2 means march etc.
            val messageDate = ""+dayOfMonth + "/" + (month + 1) + "/" + year
            showDialogEditData(messageDate);
        }


    }

    private fun showDialogEditData(messageDate:String){
        val dialogTitle = getString(R.string.name_of_title);
        val positiveButtonTitle = getString(R.string.create_event);

        val builder = AlertDialog.Builder(this);
        val editText = EditText(this);

        editText.setText(messageDate);

        builder.setTitle(dialogTitle);
        builder.setView(editText);

        builder.setPositiveButton(positiveButtonTitle){
            dialog, i ->
            val saveDataCalendarIntent = Intent (this, Save_Data_Calendar::class.java);
            startActivity(saveDataCalendarIntent)
            dialog.dismiss();
        }

        builder.create().show();
    }

}
