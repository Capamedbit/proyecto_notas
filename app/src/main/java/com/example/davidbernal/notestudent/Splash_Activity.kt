package com.example.davidbernal.notestudent

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class Splash_Activity : AppCompatActivity() {

    val SPLASH_TIME_OUT = 4000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_)
        Handler().postDelayed(object : Runnable{
            public override fun run(){
                val home = Intent(this@Splash_Activity,MainActivity::class.java)
                startActivity(home)
                finish()
            }
        },SPLASH_TIME_OUT.toLong())
    }
}
